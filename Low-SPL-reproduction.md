# Loudness, compressor/limiter in home audio reproduction

## Abstract
Do compressor/limiters perform the task for which we are using them; ie., lower dynamic range?

Working backwards, the goal is this: with a lower SPL than the performance/original program material, give the listener as much resolution as possible.

"Loudness", a psycho-acoustic effect, is a related but fundamentally different approach, where a "smile curve" is employed to exploit human ears sensitivity to mid-frequency material.

But do either, on their own or in combination to some degree, accomplish their goal? The reproduced signal is distorted by definition; and then there are unintended distortions added. These include very objectionable phase shifts around poles in filters used for loudness and loss in signal caused by the compressor/limiter step which must be overcome with more gain applied.

Are compressor/limiters and broadband filters the right approach at all? What "quality" are we trying to reintroduce at lower SPL?

* Detail?
* Presence? (as in the illusion of performance, not a mid-frequency bandpass)
* Psychoaustic flatness?
* Timbre?

I this essay, I will aim to reduce ambiguity and give reproduction technology a well-defined goal to attain.

## Compressors, limiters, and compressor/limiters

While there are many topologies, the purpose of these devices in home audio reproduction is the same: Make high amplitudes smaller, and low amplitudes higher, thereby reducing "dynamic range", or the maximum possible difference between the quietest and the loudest sound possible. Gain is applied in the final stage, the overall effect making the recording "louder".

### Professional Use in Mastering

_This section exclusively discusses mastering, but these tools are also ubiquitous in tracking._

Their cousins in mastering do an almost identical job. Infamously, in past decades, competition for the "loudest" recording has become known as the "Loudness Wars", though this name is inaccurate.

The largest mastering houses, and the smaller houses which must follow popular taste, based their investment in these techniques on the assumption (based on A/B tests with the general population) that "louder" recording will inevitably be picked as the "better" recording. Hardware retailers (home loudspeakers in particular) used this technique in retail displays decades before the Loudness Wars.

This equipment _does_ have a place in all mastering (much more dramatically in analog formats such as vinyl, but in the digital realm as well). All media in use has a smaller dynamic range than human hearing, multiple tracks in a collection such as an album should have similar average output levels, and typical home use is taken into account. All mastering desks have a pair of "low-fi" speakers meant to assure that something resembling the recording is emitted and is not strident, and mastering engineers must balance this with what they're hearing from their main monitors.

The reputation for "bad" recordings being produced during this period is not merely that compression was used, but that at least one "global" pass was made over the entire product, with tools that had recently become available. These tools required less manual engineering work (riding faders) and produced "louder" recordings with a minimum of effort. However the engineers felt about the practice or the end result, producers and publishers saw sales. The practice spread with commercial success.

At this time, it takes a human engineer to identify passages that should be quiet, and those that should be loud; it's an artistic and subjective task. However, it's expensive and dependent on the skill of the mastering engineer. A global "get it louder" run with digital effects over the entire product will be applied to commercial recordings because they sell well. Whether it is due solely to the more limited dynamic range is doubtful.